﻿using AwesomeDevEvents.API.Entities;
using Microsoft.EntityFrameworkCore;

namespace AwesomeDevEvents.API.Persistence
{
    public class DevEventsDBContext : DbContext
    {
        public DevEventsDBContext(DbContextOptions<DevEventsDBContext> options) : base(options)
        {

        }

        public DbSet<DevEvent> DevEvents { get; set; }
        public DbSet<DevEventParticipant> DevEventParticipants { get; set; }
        public DbSet<DevEventSpeaker> DevEventSpeakers { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<DevEvent>(e =>
            {
                e.HasKey(de => de.Id);

                e.Property(de => de.Title).IsRequired(true);
                e.Property(de => de.Title)
                    .HasMaxLength(200)
                    .HasColumnType("varchar(200)");

                e.Property(de => de.Description).IsRequired(false);
                e.Property(de => de.Description)
                    .HasMaxLength(200)
                    .HasColumnType("varchar(200)");

                e.Property(de => de.StartDate).IsRequired(true);
                e.Property(de => de.EndDate).IsRequired(true);

                e.HasMany(de => de.Speakers).WithOne()
                    .HasForeignKey(s => s.DevEventId);

            });

            builder.Entity<DevEventParticipant>(e =>
            {
                e.HasKey(de => de.Id);

                e.Property(de => de.FullName).IsRequired(true);
                e.Property(de => de.FullName)
                    .HasMaxLength(200)
                    .HasColumnType("varchar(200)");

                e.Property(de => de.Phone).IsRequired(true);
                e.Property(de => de.Phone)
                    .HasMaxLength(100)
                    .HasColumnType("varchar(100)");

                e.Property(de => de.Email).IsRequired(true);
                e.Property(de => de.Email)
                    .HasMaxLength(100)
                    .HasColumnType("varchar(100)");

                e.Property(de => de.Address).IsRequired(true);
                e.Property(de => de.Address)
                    .HasMaxLength(200)
                    .HasColumnType("varchar(200)");

                e.Property(de => de.Document).IsRequired(true);
                e.Property(de => de.Document)
                    .HasMaxLength(50)
                    .HasColumnType("varchar(50)");

                e.Property(de => de.Age).IsRequired(false);
                e.Property(de => de.Age)
                    .HasMaxLength(50)
                    .HasColumnType("varchar(50)");

            });

            builder.Entity<DevEventSpeaker>(e =>
            {
                e.HasKey(dev => dev.Id);

                e.Property(dev => dev.Name).IsRequired(true);
                e.Property(dev => dev.Name)
                    .HasMaxLength(200)
                    .HasColumnType("varchar(200)");

                e.Property(dev => dev.TalkTitle).IsRequired(true);
                e.Property(dev => dev.TalkTitle)
                    .HasMaxLength(200)
                    .HasColumnType("varchar(200)");

                e.Property(dev => dev.TalkDescription).IsRequired(false);
                e.Property(dev => dev.TalkDescription)
                    .HasMaxLength(450)
                    .HasColumnType("varchar(450)");

            });
        }
    }
}
