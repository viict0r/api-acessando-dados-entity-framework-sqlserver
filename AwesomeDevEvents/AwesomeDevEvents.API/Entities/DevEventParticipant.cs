﻿namespace AwesomeDevEvents.API.Entities
{
    public class DevEventParticipant
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email{ get; set; }
        public string Address { get; set; }
        public string Document { get; set; }
        public string Age{ get; set; }

        public Guid DevEventId { get; set; }
    }
}
